process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome";
process.env.NODE_ENV = "test";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const assert = require("assert");
const server = require("../server");
const pool = require("../src/DataLogic/databaseConnection");

chai.should();
chai.use(chaiHttp);

const CLEAR_DB = "DELETE IGNORE FROM `user`";
const ADD_USER = "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES('1', 'Ruben', 'van de Rande', 'rande.ruben@student.avans.nl','2149227', 'secret'), ('2', 'Luuk', 'Jansen', 'luukjansen@student.avans.nl', '5514244', 'secret')";

describe("authentication/user tests", () => {
  	before((done) => {
    	pool.query(CLEAR_DB, (err, rows, fields) => {
      		if (err) {
        		console.log("clearDatabase: " + err);
        		done(err);
      		} else {
        		done();
      		}
    	});
  	});

  	before((done) => {
		pool.query(ADD_USER, (err, rows, fields) => {
			if (err) {
				console.log("addUser: " + err);
				done(err);
			} else {
				done();
			}
		});
  	});
	
	describe("UC-101: registration tests", () => { 
		it("TC-101-1: required field is missing", (done) => {
			chai.request(server).post("/api/register").send({
				lastname: "Jannes",
				email: "rickjannes@gmail.com",
				studentnr: 2144663,
				password: "secret",
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(400);
				res.body.message.should.be.a("string").that.contains("firstname is invalid or missing");
				done();
			});
		});
		
		it("TC-101-2: email invalid", (done) => {
			chai.request(server).post("/api/register").send({
				firstname: "Rick",
				lastname: "Jannes",
				studentnr: 2144663,
				password: "secret",
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(400);
				res.body.message.should.be.a("string");
				done();
			});
		});

		it("TC-101-3: password invalid", (done) => {
			chai.request(server).post("/api/register").send({
				firstname: "Rick",
				lastname: "Jannes",
				email: "rickjannes@gmail.com",
				studentnr: 2144663,
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(400);
				res.body.message.should.be.a("string");
				done();
			});
		});
		
		it("TC-101-4: user already exists", (done) => {
			chai.request(server).post("/api/register").send({
				firstname: "Ruben",
				lastname: "van de Rande",
				email: "rande.ruben@student.avans.nl",
				studentnr: 2144663,
				password: "secret",
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(400);
				done();
			});
		});

		it("TC-101-5: user successfully registered", (done) => {
			chai.request(server).post("/api/register").send({
				firstname: "Lick",
				lastname: "Hannes",
				email: "newmail@gmail.com",
				studentnr: 2144663,
				password: "secret",
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(200);
				res.body.should.be.a("object");
				res.body.should.have.property("token").which.is.a("string");
				res.body.should.have.property("username").which.is.a("string");
				done();
			});
		});
	
		describe("UC-102: login", () => {
			it("TC-102-1: required field missing", (done) => {
			  	chai.request(server).post("/api/login").send({
					password: "secret",
				}).end((err, res) => {
					assert.ifError(err);
					res.should.have.status(400);
					res.body.message.should.be.a("string");
					done();
				});
			});

			it("TC-102-2: invalid emailadress", (done) => {
			  	chai.request(server).post("/api/login").send({
				  	email: "wrongemailadress@student.avans.nl",
				  	password: "secret",
				}).end((err, res) => {
					console.log(err);
				  	assert.ifError(err);
				  	res.should.have.status(400);
					res.body.message.should.be.a("string");
					done();
				});
			});

			it("TC-102-3: invalid password", (done) => {
			  	chai.request(server).post("/api/login").send({
				  	email: "rande.ruben@student.avans.nl",
				  	password: "wrongpassword",
				}).end((err, res) => {
				  	assert.ifError(err);
				  	res.should.have.status(400);
				  	res.body.message.should.be.a("string");
					done();
				});
			});

			it("TC-102-4: user does not exists", (done) => {
			  	chai.request(server).post("/api/login").send({
				  	email: "wrongmail@student.avans.nl",
				  	password: "secret",
				}).end((err, res) => {
					console.log(err);
				  	assert.ifError(err);
				  	res.should.have.status(400);
				  	res.body.message.should.be.a("string");
				  	done();
				});
			});

			it("TC-102-5: succesfully logged in", (done) => {
			  	chai.request(server).post("/api/login").send({
				  email: "rande.ruben@student.avans.nl",
				  password: "secret",
				}).end((err, res) => {
					const response = res.body;
				  	assert.ifError(err);
				  	res.should.have.status(200);
				  	res.body.should.be.a("object");
				  	response.should.have.property("token").which.is.a("string");
				  	response.should.have.property('username').which.is.a('string')
				  	done();
				});
			});
		});
	});
});
