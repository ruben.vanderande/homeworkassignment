process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const assert = require("assert");
const server = require("../server");
const jwt = require("jsonwebtoken");
const pool = require("../src/DataLogic/databaseConnection");

chai.should();
chai.use(chaiHttp);

const CLEAR_DB = "DELETE IGNORE FROM `studenthome`";
const ADD_HOMES = "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES ('1', 'Studentenhuis de lange Jan', 'Achthoevenstraat', 45, 1,'5071AP','0621315222','Udenhout'), ('2', 'Studentenhuis de Ketel', 'SintJanstraat', 45, 2, '6039DD','0635736686','Udenhout')";
const ADD_HOME_ADMIN = "INSERT INTO `home_administrators` (`UserId`, `StudenthomeID`) VALUES (1, 1)";

describe("studentHome tests", function () {
		before((done) => {
			pool.query(CLEAR_DB, (err, rows, fields) => {
				if (err) {
					console.log("clearDatabase: " + err);
					done(err);
				} else {
					done();
				}
			});
		});

		before((done) => {
			pool.query(ADD_HOMES, (err, rows, fields) => {
				if (err) {
					console.log("addStudentHousing: " + err);
					done(err);
				} else {
					done();
				}
			});
		});

		before((done) => {
			pool.query(ADD_HOME_ADMIN, (err, rows, fields) => {
			  	if (err) {
					console.log("addStudentHousingadmin: " + err);
					done(err);
			  	} else {
					done();
			  	}
			});
		});

	describe("UC-201: add studenthome tests", function () {
		jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			it("TC-201-1: required field is missing", (done) => {
				chai.request(server).post("/api/studenthome").set("authorization", "Bearer " + token).send({
					address: "StudenthomeTeststreet",
					houseNr: 3,
					postalCode: "3333DD",
					phoneNr: "0673677367",
					city: "Citytest",
				}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(400);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		
		
			it("TC-201-2: invalid postalcode", (done) => {
				chai.request(server).post("/api/studenthome").set("authorization", "Bearer " + token).send({
					name: "StudenthomeTesthome",
					address: "StudenthomeTeststreet",
					houseNr: 3,
					postalCode: "",
					phoneNr: "0673677367",
					city: "Citytest",
				}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(400);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		});
		/*
		
			it("TC-201-3: invalid phonenumber", (done) => {
				chai.request(server).post("/api/studenthome").set("authorization", "Bearer " + token).send({
					name: "StudenthomeTesthome",
					address: "StudenthomeTeststreet",
					houseNr: 3,
					postalCode: "3333DD",
					phoneNr: "067D677367",
					city: "Citytest",
				}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(400);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});

		it("TC-201-4: studenthome already exists", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).post("/api/studenthome").set("authorization", "Bearer " + token).send({
					name: "Studentenhuis de lange Jan",
					address: "Achthoevenstraat",
					houseNr: 45,
					postalCode: "5071AP",
					phoneNr: "0621315222",
					city: "Udenhout",
				}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(400);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		});
		
		it("TC-201-5: not logged in", (done) => {
			chai.request(server).post("/api/studenthome").send({
				name: "StudenthomeTesthome",
				address: "StudenthomeTeststreet",
				houseNr: 3,
				postalCode: "3333DD",
				phoneNr: "0673677367",
				city: "Citytest",
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(401);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "datetime");
				res.body.message.should.be.a("string");
				done();
			});
		});

		it("TC-201-6: studenthome succesfully added", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).post("/api/studenthome").set("authorization", "Bearer " + token).send({
					name: "StudenthomeTesthome",
					address: "StudenthomeTeststreet",
					houseNr: 3,
					postalCode: "3333DD",
					phoneNr: "0673677367",
					city: "Citytest",
				}).end((err, res) => {
					let { status, result } = res.body;
					assert.ifError(err);
					res.should.have.status(200);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("status", "result");
					status.should.be.a("string").that.contains("success");
					result.should.be.an("Object").that.contains({
						name: "StudenthomeTesthome",
						address: "StudenthomeTeststreet",
						houseNr: 3,
						postalCode: "3333DD",
						phoneNr: "0673677367",
						city: "Citytest",
					});
				done();
				});
			});
		});
	});

	describe("UC-202: overview studenthomes & UC-203: details from studenthomes", function () {
		before((done) => {
			pool.query(clearDatabase, (err, rows, fields) => {
				if (err) {
					console.log("clearDatabase: " + err);
					done(err);
				} else {
					done();
				}
			});
		});

		it("TC-202-1: show 0 studenthomes", (done) => {
			chai.request(server).get("/api/studenthome").end((err, res) => {
				let { status, result } = res.body;
				assert.ifError(err);
				res.should.have.status(200);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("status", "result");
				status.should.be.a("string").that.contains("success");
				result.should.be.a("array").that.has.lengthOf(0);
				done();
			});
		});
		
		it("TC-202-2: show 2 studenthomes", (done) => {
			pool.query(addStudentHousing, (error, resultAdd) => {
				if (error) {
					done(err);
				} else if (resultAdd) {
					chai.request(server).get("/api/studenthome?city=Udenhout").end((err, res) => {
						let { status, result } = res.body;
						assert.ifError(err);
						res.should.have.status(200);
						res.should.be.an("object");
						res.body.should.be.an("object").that.has.all.keys("status", "result");
						status.should.be.a("string").that.contains("success");
						result.should.be.an("array").that.has.lengthOf(2);
						done();
					});
				}
			});
		});

		it("TC-202-3: show studenthomes with search term by non-existent city", (done) => {
			chai.request(server).get("/api/studenthome?city=NewYork").end((err, res) => {
				let { message, error } = res.body;
				assert.ifError(err);
				res.should.have.status(404);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "error");
				message.should.be.a("string");
				error.should.be.a("string");
				done();
			});
		});

		it("TC-202-4: show studenthomes with search term by non-existent name", (done) => {
			chai.request(server).get("/api/studenthome?nameCodingHome").end((err, res) => {
				let { message, error } = res.body;
				assert.ifError(err);
				res.should.have.status(404);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "error");
				message.should.be.a("string");
				error.should.be.a("string");
				done();
			});
		});

		it("TC-202-5: show studenthomes with search term on existing city", (done) => {
			chai.request(server).get("/api/studenthome?city=Udenhout").end((err, res) => {
				let { status, result } = res.body;
				assert.ifError(err);
				res.should.have.status(200);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("status", "result");
				status.should.be.a("string").that.contains("success");
				result.should.be.a("array");
				result.forEach((studentHome) => {
					studentHome.should.be.an("Object").that.contains({ City: "Udenhout" });
				});
				done();
			});
		});

		it("TC-202-6: show studenthomes with search term by existing name", (done) => {
			chai.request(server).get("/api/studenthome?name=Studentenhuis de lange Jan").end((err, res) => {
				let { status, result } = res.body;
				assert.ifError(err);
				res.should.have.status(200);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("status", "result");
				status.should.be.a("string").that.contains("success");
				result.should.be.a("array");
				result.forEach((studentHome) => {
					studentHome.should.be.an("Object").that.contains({ Name: "Studentenhuis de lange Jan" });
				});
				done();
			});
		});

		it("TC-203-1: studenthomeId does not exist", (done) => {
			chai.request(server).get("/api/studenthome/-1").end((err, res) => {
				let { message, error } = res.body;
				assert.ifError(err);
				res.should.have.status(404);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "error");
				message.should.be.a("string").that.contains("No studentHome found for id:");
				error.should.be.a("string");
				done();
			});
		});

		it("TC-203-2: studenthomeId does exist", (done) => {
			chai.request(server).get("/api/studenthome/1").end((err, res) => {
				let { status, result } = res.body;
				assert.ifError(err);
				res.should.have.status(200);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("status", "result");
				status.should.be.a("string").that.contains("success");
				result[0].should.be.an("Object").that.contains({
					ID: 1,
					Name: "Studentenhuis de lange Jan",
					Address: "Achthoevenstraat",
					House_Nr: 45,
					UserID: 1,
					Postal_Code: "5071AP",
					Telephone: "0621315222",
					City: "Udenhout",
				});
				done();
			});
		});
	});
		
	describe("UC-204: alter studenthomes", function () {
		it("TC-204-1: required field missing", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).put("/api/studenthome/1").set("authorization", "Bearer " + token).send({
					address: "StudenthomeTeststreet",
					houseNr: 3,
					postalCode: "3333DD",
					phoneNr: "0673677367",
					city: "Citytest",
				}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(400);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		});
		
		it("TC-204-2: invalid postalcode", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).put("/api/studenthome/1").set("authorization", "Bearer " + token).send({
					name: "StudenthomeTesthome",
					address: "StudenthomeTeststreet",
					houseNr: 3,
					postalCode: "12",
					phoneNr: "0673677367",
					city: "Citytest",
				}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(400);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		});
		
		it("TC-204-3: invalid phonenumber", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).put("/api/studenthome/1").set("authorization", "Bearer " + token).send({
					name: "Studenhometesthome",
					address: "Studenhometeststreet",
					houseNr: 3,
					postalCode: "3333DD",
					phoneNr: "wrongphonenumber",
					city: "Citytest",
				}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(400);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		});
		
		it("TC-204-4: studenthome does not exist", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).put("/api/studenthome/-1").set("authorization", "Bearer " + token).send({
					name: "StudenthomeTesthome",
					address: "StudenthomeTeststreet",
					houseNr: 3,
					postalCode: "3333DD",
					phoneNr: "0673677367",
					city: "Citytest",
				}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(404);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		});
		
		it("TC-204-5: not logged in", (done) => {
			chai.request(server).put("/api/studenthome/1").send({
				name: "Studenhometesthome",
				address: "Studenhometeststreet",
				houseNr: 3,
				postalCode: "3333DD",
				phoneNr: "0673677367",
				city: "Citytest",
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(401);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "datetime");
				res.body.message.should.be.a("string");
				done();
			});
		});

		it("TC-204-6: studenthome succesfully altered", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).put("/api/studenthome/1").set("authorization", "Bearer " + token).send({
					name: "Studenhometesthome",
					address: "Studenhometeststreet",
					houseNr: 4,
					postalCode: "3333DD",
					phoneNr: "0673677367",
					city: "Citytest",
				}).end((err, res) => {
					let { status, result } = res.body;
					assert.ifError(err);
					res.should.have.status(200);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("status", "result");
					status.should.be.a("string").that.contains("success");
					result.should.be.an("object").that.eql({
						id: 1,
						name: "Studenhometesthome",
						address: "Studenhometeststreet",
						houseNr: 4,
						postalCode: "3333DD",
						phoneNr: "0673677367",
						city: "Citytest",
					});
					done();
				});
			});
		});
	});
		
	describe("UC-205: remove studenthome", function () {
		it("TC-205-1: studenthome already exists", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).delete("/api/studenthome/-1").set("authorization", "Bearer " + token).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(404);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		});
		
		it("TC-205-2: not logged in", (done) => {
			chai.request(server).delete("/api/studenthome/1").end((err, res) => {
				assert.ifError(err);
				res.should.have.status(401);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "datetime");
				res.body.message.should.be.a("string");
				done();
			});
		});

		it("TC-205-3: user is not the owner", (done) => {
			jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).delete("/api/studenthome/1").set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" })).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(401);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
					message.should.be.a("string");
					error.should.be.a("string");
					done();
				});
			});
		});
		
		it("TC-205-4: studenthome succesfully removed", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).delete("/api/studenthome/2").set("authorization", "Bearer " + token).end((err, res) => {
					let { status, result } = res.body;
					assert.ifError(err);
					res.should.have.status(200);
					res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("status", "result");
					status.should.be.a("string").that.contains("success");
					result.should.be.a("array").that.has.lengthOf(1);
					result[0].should.be.an("Object").that.contains({ ID: 2 });
					done();
				});
			});
		});

		*/
	});
});