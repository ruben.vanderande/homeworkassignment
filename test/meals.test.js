process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const assert = require("assert");
const server = require("../server");
const jwt = require("jsonwebtoken");
const pool = require("../src/DataLogic/databaseConnection");

chai.should();
chai.use(chaiHttp);	


const CLEAR_DB = "DELETE IGNORE FROM `meal`";
const ADD_HOME = "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES ('1', 'Studentenhuis de lange Jan', 'Achthoevenstraat', 45, 1,'5071AP','0621315222','Udenhout')";
const ADD_MEAL = "INSERT INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES ('1', 'Hamburger', 'Een lekkere cheeseburger met meerdere lagen.', 'Buns, hamburger, Sla, Zout, Tomaat, Augurk', 'Geen allergiën','2021-05-25 12:00','2021-07-25 12:00', 2.00, 1, 1, 10)";

/*

describe("meal tests", function () {
  	before((done) => {
		console.log("start clear");
    	pool.query(CLEAR_DB, (err, rows, fields) => {
      		if (err) {
				console.log("clearDatabase error: " + err);
        		done(err);
      		} else {
        		done();
      		}
    	});
  	});

  	before((done) => {
    	pool.query(ADD_HOME, (err, rows, fields) => {
			console.log("start addhome");
      		if (err) {
        		console.log("addStudentHousing: " + err);
        		done(err);
      		} else {
        		done();
      		}
    	});
  	});

  	before((done) => {
    	pool.query(ADD_MEAL, (err, rows, fields) => {
			console.log("start addmeal");
      		if (err) {
        		console.log("addMeals: " + err);
        		done(err);
      		} else {
        		done();
      		}
    	});
  	});
	
	describe("UC-301: adding meals", function () {
		it("TC-301-1: required field missing", (done) => {
		  	jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).post("/api/studenthome/1/meal").set("authorization", "Bearer " + token).send({
					description: "Fish with rice",
					ingredients: ["Salmon", "Sushimi", "Seagrass", "Rice"],
					allergyInfo: "Fish allergie",
					offerDate: "2021-05-15T18:00:00",
					price: "15.00",
			  	}).end((err, res) => {
					let { message, error } = res.body;
					assert.ifError(err);
					res.should.have.status(400);
					message.should.be.a("string");
					done();
			  	});
		  	});
		});

		it("TC-301-2: not logged in", (done) => {
			chai.request(server).post("/api/studenthome/1/meal").send({
				name: "Sushi",
				description: "Fish with rice",
				ingredients: ["Salmon", "Sushimi", "Seagrass", "Rice"],
				allergyInfo: "Fish allergie",
				offerDate: "2021-05-15T18:00:00",
				price: "15.00",
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(401);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "datetime");
				res.body.message.should.be.a("string");
				done();
			});
		});

		it("TC-301-3: not the owner of the data", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
				chai.request(server).post("/api/studenthome/1/meal").set("authorization", "Bearer " + token).send({
				  	name: "Sushi",
				  	description: "Fish with rice",
				  	ingredients: ["Salmon", "Sushimi", "Seagrass", "Rice"],
				  	allergyInfo: "Fish allergie",
				  	offerDate: "2021-05-15T18:00:00",
				  	price: "15.00",
				}).end((err, res) => {
					let { status, result } = res.body;
					assert.ifError(err);
					res.should.have.status(200);
					res.should.be.an("object");
				  	res.body.should.be.an("object").that.has.all.keys("status", "result");
	  				status.should.be.a("string").that.contains("success");
				  	result.should.be.an("object").that.has.all.keys("id", "name", "description", "ingredients", "allergyInfo", "offerDate", "price", "creationDate");
	  				done();
				});
			});
		});
	});
	  
	describe("UC-302: alter meal", function () {
		it("TC-302-1: required field missing", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			  	chai.request(server).put("/api/studenthome/1/meal/1").set("authorization", "Bearer " + token).send({
				  	description: "Een lekkere cheeseburger met meerdere lagen.",
				  	ingredients: ["Buns", "hamburger", "Sla", "Zout", "Tomaat", "Augurk"],
				  	allergyInfo: "Geen allergiën",
				  	offerDate: "2020-01-02 10:10",
				  	price: "3.00",
				}).end((err, res) => {
					let { message, error } = res.body;
				  	assert.ifError(err);
				  	res.should.have.status(400);
				  	res.should.be.an("object");
					res.body.should.be.an("object").that.has.all.keys("message", "error");
				  	message.should.be.a("string");
				  	error.should.be.a("string");
				  	done();
				});
			});
		});

		it("TC-302-2: not logged in", (done) => {
			chai.request(server).put("/api/studenthome/1/meal/1").send({
				name: "Hamburger",
				description: "Een lekkere cheeseburger met meerdere lagen.",
				ingredients: ["Buns", "hamburger", "Sla", "Zout", "Tomaat", "Augurk"],
				allergyInfo: "Geen allergiën",
				offerDate: "2020-01-02 10:10",
				price: "3.00",
			}).end((err, res) => {
				assert.ifError(err);
				res.should.have.status(401);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "datetime");
	  			res.body.message.should.be.a("string");
	  			done();
			});
		});

		//----------

		it("TC-302-3: not the owner of the data", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			  	chai.request(server).put("/api/studenthome/1/meal/1").set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" })).send({
				  	name: "Hamburger",
				  	description: "Een lekkere cheeseburger met meerdere lagen.",
				  	ingredients: ["Buns", "hamburger", "Sla", "Zout", "Tomaat", "Augurk"],
				  	allergyInfo: "Geen allergiën",
				  	offerDate: "2020-01-02 10:10",
				  	price: "3.00",
				}).end((err, res) => {
				  	assert.ifError(err);
				  	res.should.have.status(401);
				  	res.should.be.an("object");
				 	res.body.should.be.an("object").that.has.all.keys("error", "message");
				  	res.body.message.should.be.a("string");
				  	done();
				});
			});
		});

		
	  
		it("TC-302-4: meal does not exist", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			  	chai.request(server).put("/api/studenthome/1/meal/-1").set("authorization", "Bearer " + token).send({
					name: "Hamburger",
					description: "Een lekkere cheeseburger met meerdere lagen.",
					ingredients: ["Buns", "hamburger", "Sla", "Zout", "Tomaat", "Augurk"],
					allergyInfo: "Geen allergiën",
					offerDate: "2020-01-02 10:10",
					price: "3.00",
				}).end((err, res) => {
					let { message, error } = res.body;
				  	assert.ifError(err);
				  	res.should.have.status(404);
				  	res.should.be.an("object");
				  	res.body.should.be.an("object").that.has.all.keys("message", "error");
				  	message.should.be.a("string").that.contains("No meal found for id:");
				  	error.should.be.a("string");
				  	done();
				});
			});
		});
	  
		it("TC-302-5: list of meals returned", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			  	chai.request(server).put("/api/studenthome/1/meal/1").set("authorization", "Bearer " + token).send({
				  	id: null,
					name: "Hamburger",
					description: "Een lekkere cheeseburger met meerdere lagen.",
					ingredients: ["Buns", "hamburger", "Sla", "Zout", "Tomaat", "Augurk"],
					allergyInfo: "Geen allergiën",
					offerDate: "2020-01-02 10:10",
					price: "3.00",
				}).end((err, res) => {
					let { status, result } = res.body;
				  	assert.ifError(err);
				  	res.should.have.status(200);
				  	res.should.be.an("object");
				  	res.body.should.be.an("object").that.has.all.keys("status", "result");
				  	status.should.be.a("string").that.contains("success");
				  	result.should.be.a("object");
				  	done();
				});
			});
		});
	});
	  
	describe("UC-303: request a list of meals & UC-304: request details of a meal", function () {
		it("TC-303-1: list of meals returned", (done) => {
			chai.request(server).get("/api/studenthome/1/meal").end((err, res) => {
				assert.ifError(err);
				let { status, result } = res.body;
				res.should.have.status(200);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("status", "result");
				status.should.be.a("string").that.contains("success");
				result.should.be.a("array");
				done();
			});
		});

		it("TC-304-1: meal does not exist", (done) => {
			chai.request(server).get("/api/studenthome/1/meal/-1").end((err, res) => {
				let { message, error } = res.body;
				assert.ifError(err);
				res.should.have.status(404);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "error");
				message.should.be.a("string").that.contains("No meal found for id:");
				error.should.be.a("string");
				done();
			});
		});

		it("TC-304-2: meal details returned", (done) => {
			chai.request(server).get("/api/studenthome/1/meal/1").end((err, res) => {
				let { status, result } = res.body;
				assert.ifError(err);
				res.should.have.status(200);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("status", "result");
				status.should.be.a("string").that.contains("success");
				result.should.be.a("array");
				done();
			});
		});
	});

	describe("UC-305: remove meal", function () {
		it("TC-305-1: required field missing", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			  	chai.request(server).delete("/api/studenthome/-1/meal/1").set("authorization", "Bearer " + token).end((err, res) => {
					let { message, error } = res.body;
				  	assert.ifError(err);
				  	res.should.have.status(404);
				  	res.should.be.an("object");
				  	res.body.should.be.an("object").that.has.all.keys("message", "error");
				  	message.should.be.a("string");
				  	error.should.be.a("string");
				  	done();
				});
			});
		});
	  
		it("TC-305-2: not logged in", (done) => {
			chai.request(server).delete("/api/studenthome/1/meal/1").end((err, res) => {
				assert.ifError(err);
				res.should.have.status(401);
				res.should.be.an("object");
				res.body.should.be.an("object").that.has.all.keys("message", "datetime");
				res.body.message.should.be.a("string");
				done();
			});
		});

		it("TC-305-3: not the owner of the data", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			  	chai.request(server).delete("/api/studenthome/1/meal/1").set( "authorization", "Bearer " + jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" })).end((err, res) => {
				  	assert.ifError(err);
				  	res.should.have.status(401);
				  	res.should.be.an("object");
				  	res.body.should.be.an("object").that.has.all.keys("error", "message");
				  	res.body.message.should.be.a("string");
				  	done();
				});
			});
		});
	  
		it("TC-305-4: meal does not exist", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			  	chai.request(server).delete("/api/studenthome/1/meal/-1").set("authorization", "Bearer " + token).end((err, res) => {
					let { message, error } = res.body;
				  	assert.ifError(err);
				  	res.should.have.status(404);
				  	res.should.be.an("object");
				  	res.body.should.be.an("object").that.has.all.keys("message", "error");
				  	message.should.be.a("string");
				  	error.should.be.a("string");
				  	done();
				});
			});
		});
	  
		it("TC-305-5: meal succesfully removed", (done) => {
			jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
			  	chai.request(server).delete("/api/studenthome/1/meal/1").set("authorization", "Bearer " + token).end((err, res) => {
					let { status, result } = res.body;
				  	assert.ifError(err);
				  	res.should.have.status(200);
				  	res.should.be.an("object");
				  	res.body.should.be.an("object").that.has.all.keys("status", "result");
				  	status.should.be.a("string").that.contains("success");
				  	result.should.be.a("string");
				  	done();
				});
			});
		});
	});

	describe("UC-401: sign up for meal", function () {
		it("TC-401-1: not logged in", (done) => {
		});

		it("TC-401-2: meal does not exist", (done) => {
		});

		it("TC-401-3: succesfully signed up", (done) => {
		});
	});

	describe("UC-402: sign off for meal", function () {
		it("TC-402-1: not logged in", (done) => {
		});
	  
		it("TC-402-2: meal does not exist", (done) => {
		});

		it("TC-402-3: sign up does not exist", (done) => {
		});

		it("TC-402-4: succesfully signed off", (done) => {
		});	
	});

	describe("UC-403: request a list of participants", function () {
		it("TC-403-1: not logged in", (done) => {
		});
	  
		it("TC-403-2: meal does not exist", (done) => {
		});

		it("TC-403-3: list of participants returned", (done) => {
		});
	});

	describe("UC-404: request participant details", function () {
		it("TC-404-1: not logged in", (done) => {
		});
	  
		it("TC-404-2: participant does not exist", (done) => {
		});

		it("TC-404-3: participant contact details returned", (done) => {
		});

		//--------
	});
});

*/