const express = require("express");
const mealRouter = express.Router();
const mealItem = require("../ApplicationLogic/mealController");
const authentication = require("../ApplicationLogic/authenticationController");

//UC-301
mealRouter.post("/studenthome/:homeId/meal", authentication.validateToken, mealItem.checkMeal, mealItem.mealAdd);

//UC-302
mealRouter.put("/studenthome/:homeId/meal/:mealId", authentication.validateToken, mealItem.checkMeal, mealItem.updateMeal);

//UC-305
mealRouter.delete("/studenthome/:homeId/meal/:mealId", authentication.validateToken, mealItem.deleteMeal);

//UC-303
mealRouter.get("/studenthome/:homeId/meal", mealItem.getAllMeals);

//UC-304
mealRouter.get("/studenthome/:homeId/meal/:mealId", mealItem.getMealId);

module.exports = mealRouter;