const express = require("express");
const userRouter = express.Router();
const user = require("../ApplicationLogic/userController.js");
const authentication = require("../ApplicationLogic/authenticationController");

//UC-401
userRouter.post("/studenthome/:homeId/meal/:mealId/signup", authentication.validateToken, user.addUserToMeals);

//UC-402
userRouter.put("/studenthome/:homeId/meal/:mealId/signoff", authentication.validateToken, user.removeUsersFromMeals);

//UC-403
userRouter.get("/meal/:mealId/participants", user.getUsersFromMeal);

//UC-404
userRouter.get("/meal/:mealId/participants/:participantId", user.getUserFromMeal);

module.exports = userRouter;
