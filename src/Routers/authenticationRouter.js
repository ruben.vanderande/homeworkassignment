const express = require("express");
const authenticationRouter = express.Router();
const authentication = require("../ApplicationLogic/authenticationController");

//UC-102
authenticationRouter.post("/login", authentication.validateLogin, authentication.login);

//UC-101
authenticationRouter.post("/register", authentication.validateRegister, authentication.register);

module.exports = authenticationRouter;