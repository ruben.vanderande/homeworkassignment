const express = require("express");
const housingRouter = express.Router();
const studentHome = require("../ApplicationLogic/studentHousingController");
const authentication = require("../ApplicationLogic/authenticationController");

//UC-201
housingRouter.post("/studenthome", authentication.validateToken, studentHome.checkStudentHome, studentHome.studentHomeAdd);

//UC-206
housingRouter.put("/studenthome/:homeId/user", authentication.validateToken, studentHome.addUserToStudentHome);

//UC-204
housingRouter.put("/studenthome/:homeId", authentication.validateToken, studentHome.checkStudentHome, studentHome.updateStudentHome);

//UC-205
housingRouter.delete("/studenthome/:homeId", authentication.validateToken, studentHome.deleteStudentHome);

//UC-202
housingRouter.get("/studenthome", studentHome.getAllStudentHomes);

//UC-203
housingRouter.get("/studenthome/:homeId", studentHome.getStudentHome);

module.exports = housingRouter;




/*
//UC-201
router.post("/studenthome", studentHome.checkStudentHome, studentHome.StudentHomeAdd);

//UC-204
router.put("/studenthome/:homeId", studentHome.checkStudentHome, studentHome.updateStudentHome);
*/