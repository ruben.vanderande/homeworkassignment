const pool = require("../DataLogic/databaseConnection");

module.exports = {
	//UC-301
  	addMeal(studentHomeId, userId, meal, callback) {
		console.log("addMeal called");
    	meal.creationDate = new Date();
    	let { name, description, ingredients, allergyInfo, offerDate, price, creationDate } = meal;
		let sqlQuery = "INSERT INTO meal (Name, Description, Ingredients, Allergies, CreatedOn, OfferedOn, Price, UserID, StudenthomeID, MaxParticipants) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        	let error = {
          		message: errCon.message,
          		errCode: 400,
        	   };
        	callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, [name, description, ingredients.join(", "), allergyInfo, creationDate, offerDate, price, userId, studentHomeId, 0], (error, results, fields) => {
					connection.release();
					
					if (error) {
						console.log(error);
						const err = {
							message: "query error: " + error.message,
							errCode: 400,
						};
						callback(err, undefined);
					};
					if (results) {
						callback(undefined, results);
					};
          		});
      		};
    	});
  	},
	
	//UC-302
  	updateMeal(studentHomeId, userId, mealId, meal, callback) {
		console.log("updateMeal called");
    	let { name, description, ingredients, allergyInfo, offerDate, price } = meal;
    	let sqlQuery = "UPDATE meal SET Name = ?, Description = ?, Ingredients = ?, Allergies = ?, OfferedOn = ?, Price = ? WHERE ID = ? AND StudentHomeID = ? AND UserID = ?";

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, [name, description, ingredients.join(", "), allergyInfo, offerDate, price, mealId, studentHomeId, userId], (error, results, fields) => {
            		connection.release();
            		if (error) {
              			const err = {
                			message: "query error: " + error.message,
                			errCode: 400,
              			};
              			callback(err, undefined);
            		};
            		if (results) {
              			if (!results.affectedRows) {
                			const err = {
                  				message: userId + " not authorized",
                  				errCode: 401,
                			};
                			callback(err, undefined);
              			} else {
                			callback(undefined, results);
              			};
            		};
          		});
      		};
    	});
  	},
	
	//UC-305
	deleteMeal(studentHomeId, userId, mealId, callback) {
		console.log("deleteMeal called");
    	let sqlQuery = "DELETE FROM meal WHERE ID = " + mealId + " AND StudenthomeID = " + studentHomeId + " AND UserID = " + userId;

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, (error, results, fields) => {
          			connection.release();

          			if (error) {
            			const err = {
              				message: "query error: " + error.message,
              				errCode: 400,
            			};
            			callback(err, undefined);
          			};
          			if (results) {
            			if (!results.affectedRows) {
              				const err = {
                				message: userId + " not authorized",
                				errCode: 401,
              				};
              				callback(err, undefined);
            			} else {
              				results.message = "meal deleted";
              				callback(undefined, results);
            			};
          			};
        		});
      		};
    	});
  	},
	
	//UC-302 & UC-303 & UC-305 & UC-401 & UC-402
  	getMeals(studentHomeId, callback) {
		console.log("getStudentHomeMeals called");
    	let sqlQuery = "SELECT * FROM meal WHERE StudentHomeID = " + studentHomeId;

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, (error, results, fields) => {
					connection.release();

					if (error) {
						const err = {
							message: "query error: " + error.message,
							errCode: 400,
						};
						callback(err, undefined);
					};
					if (results) {
						if (!results.length) {
							const err = {
								message: "no meals found with: " + studentHomeId,
								errCode: 404,
							};
							callback(err, undefined);
						} else {
							results.forEach(result => {
								result.Ingredients = result.Ingredients.split(', ');
							});
							callback(undefined, results);
						};
					};
        		});
      		};
    	});
  	},
};
