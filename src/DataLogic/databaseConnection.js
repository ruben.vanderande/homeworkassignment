const mysql = require("mysql");
const databaseconfig = require("./databaseConfig").dbconfig;

const pool = mysql.createPool(databaseconfig);

pool.on("connection", function (connection) {
    console.log("connected!");
});

module.exports = pool;

//This code is meant to be used for an array database.

/*
module.exports = {
    studentenhuisDB: [
        {
            id: 1,
            naam: "Studentenhuis het lekke biertje",
            straatnaam: "John Jansenstraat",
            huisnummer: 34,
            postcode: "5082SV",
            plaats: "Tilburg",
            telefoonnummer: 0134337652,
        },
        {
            id: 2,
            naam: "Studentenhuis de heilige Harm",
            straatnaam: "Kastanjelaan",
            huisnummer: 12,
            postcode: "3452DD",
            plaats: "Tilburg",
            telefoonnummer: 0134237652,
        },
    ],

    //UC-201
    add(newStudentHome, callback) {
        newStudentHome.id = this.studentenhuisDB.length + 1;
        this.studentenhuisDB.push(newStudentHome);

        callback(undefined, newStudentHome);       
    },

    //UC-204
    update(id, updateHousingInfo, callback) {
        let updatingStudentHomeItem = this.studentenhuisDB.filter(function (e) {
          return e.id == id;
        });
    
        if (updatingStudentHomeItem.length) {
          const changedItem = this.studentenhuisDB.indexOf(updatingStudentHomeItem[0]);
          this.studentenhuisDB[changedItem].naam = updateHousingInfo.name;
          this.studentenhuisDB[changedItem].straat = updateHousingInfo.street;
          this.studentenhuisDB[changedItem].huisnummer = updateHousingInfo.housenumber;
          this.studentenhuisDB[changedItem].postcode = updateHousingInfo.postalCode;
          this.studentenhuisDB[changedItem].plaats = updateHousingInfo.city;
          this.studentenhuisDB[changedItem].telefoonnummer = updateHousingInfo.phonenumber;
    
          callback(undefined, this.studentenhuisDB[homeIndex]);
        } else {
          const err = {
            message: "Studenthome not found",
            errCode: 404,
          };
          callback(err, undefined);
        }
    },

    //UC-205
    delete(id, callback) {
        let studentHome = this.studentenhuisDB.filter(function (e) {
          return e.id == id;
        });
    
        if (studentHome.length) {
          this.studentenhuisDB.splice(homeIndex, 1);
          callback(undefined, studentHome);
        } else {
          const err = {
            message: "Studenthome not found",
            errCode: 404,
          };
          callback(err, undefined);
        }
    },

    //UC-202
    getByName(name, city, callback) {
        let studentHome = this.studentenhuisDB.filter(function (e) {
            return (
              e.name == name || e.city == city || (e.name == name && e.city == city)
            );
        });
      
        if (studentHome.length > 0 || (!name && !city)) {
        callback(undefined, studentHome);
        } else {
            let err = {
              message: "Studenthome not found",
              errCode: 404,
            };

            if (name) {
                err.message += "name: " + name;
            }

            if (city) {
                err.message += "city: " + city;
            }

            callback(err, undefined);
        }
    },

    //UC-203
    getById(id, callback) {
        let studentHome = this.studentenhuisDB.filter(function (e) {
          return e.id == id;
        });
    
        if (studentHome.length) {
          callback(undefined, studentHome);
        } else {
          const err = {
            message: "Studenthome not found",
            errCode: 404,
          };
          callback(err, undefined);
        }
    },

    serverInfo: [
        {
            naam: "Ruben van de Rande",
            studentnummer: "2149227",
            beschrijving: "This is a basic Javascript Express server, that runs on Heroku via Gitlab. It is build to learn Javascript and Javascripts serverside in term 1.4 of informatica at Avans University of Applied Sciences.",
            SonarQube: "not difined",
        },
    ],
};
*/