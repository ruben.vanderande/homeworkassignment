const pool = require("../DataLogic/databaseConnection");

module.exports = {
	//UC-401
  	addUserToMeal(studentHomeId, mealId, userId, signUpDate, callback) {
		console.log("addUserToMeal called");
    	let sqlQuery = "INSERT INTO participants (UserID, StudenthomeID, MealID, SignedUpOn) VALUES (?, ?, ?, ?)";

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		}
      		if (connection) {
        		connection.query(sqlQuery, [userId, studentHomeId, mealId, signUpDate], (error, results, fields) => {
            		connection.release();

            		if (error) {
              			const err = {
                			message: "query error: " + error.message,
                			errCode: 400,
              			};
              		callback(err, undefined);
            		}
            		if (results) {
              			callback(undefined, results);
            		}
          		});
      		}
    	});
  	},

	//UC-402
  	deleteUser(studentHomeId, userId, mealId, callback) {
		console.log("deleteUser called");
    	let sqlQuery = "DELETE FROM participants WHERE UserID = ? AND StudenthomeID = ? AND MealID = ?";

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, [userId, studentHomeId, mealId], (error, results, fields) => {
            		connection.release();

            		if (error) {
              			const err = {
                			message: "query error: " + error.message,
                			errCode: 400,
              			};
              			callback(err, undefined);
            		};
					if (results) {
						results.message = "user deleted";
						callback(undefined, results);
					};
          		});
      		};
    	});
 	},

	//uc-402 & UC-403 & UC-404
  	getAllUsers(mealId, callback) {
		console.log("getAllUsers called");
    	let sqlQuery = "SELECT * FROM participants WHERE MealID = ?";

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
    			connection.query(sqlQuery, [mealId], (error, results, fields) => {
          			connection.release();

          			if (error) {
            			const err = {
              				message: "query error: " + error.message,
              				errCode: 400,
            			};
            			callback(err, undefined);
          			};
            		if (results) {
            			callback(undefined, results);
            		};
            	});
        	};
    	});
    },

	//UC-401
    getUser(mealId, userId, callback) {
		console.log("getUser called");
      	let sqlQuery = "SELECT * FROM participants WHERE MealID = ? AND UserID = ?";
  
      	pool.getConnection((errCon, connection) => {
        	if (errCon) {
          		let error = {
            		message: errCon.message,
            		errCode: 400,
          		};
          		callback(error, undefined);
        	}
        	if (connection) {
          		connection.query(sqlQuery, [mealId, userId], (error, results, fields) => {
					connection.release();
	
					if (error) {
						const err = {
							message: "query error: " + error.message,
							errCode: 400,
						};
						callback(err, undefined);
					}
					if (results) {
						callback(undefined, results);
					}
          		});
        	}
      	});
    },
};
