const pool = require("../DataLogic/databaseConnection");

module.exports = {
	//UC-201
  	addStudentHousing(studentHome, userId, callback) {
		console.log("addStudentHousing called");
   		let { name, address, houseNr, postalCode, phoneNr, city } = studentHome;
		let sqlQuery = "INSERT INTO studenthome (Name, Address, House_Nr, UserID, Postal_Code, Telephone, City) VALUES (?, ?, ?, ?, ?, ?, ?)";

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
         			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, [name, address, houseNr, userId, postalCode, phoneNr, city], (error, results, fields) => {
            		connection.release();

            		if (error) {
              			const err = {
                			message: "query error: " + error.message,
                			errCode: 400,
              			};
              			callback(err, undefined);
            		};
            		if (results) {
              			callback(undefined, results);
            		};
          		});
      		};
    	});
  	},
	
	//UC-204
  	updateStudentHome(homeId, studentHome, userId, callback) {
		console.log("updateStudentHome called");
    	let { name, address, houseNr, postalCode, phoneNr, city } = studentHome;
		let sqlQuery = "UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?, Postal_Code = ?, Telephone = ?, City = ? WHERE ID = ? AND UserID = ?";

		pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
        			message: errCon.message,
        			errCode: 400,
        		};
        	 	callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, [name, address, houseNr, postalCode, phoneNr, city, homeId, userId], (error, results, fields) => {
            		connection.release();
            		if (error) {
              			const err = {
               				message: "query error: " + error.message,
                			errCode: 400,
              			};
              			callback(err, undefined);
            		};
            		if (results) {
              			callback(undefined, results);
            		};
          		});
      		};
    	});
  	},
	
	//UC-205
  	deleteStudentHome(homeId, userId, callback) {
		console.log("deleteStudentHome called");
    	let sqlQuery = "DELETE FROM studenthome WHERE ID = " + homeId + " AND UserID = " + userId;

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, (error, results, fields) => {
          			connection.release();
          			if (error) {
            			const err = {
              				message: "query error: " + error.message,
              				errCode: 400,
            			};
            			callback(err, undefined);
          			};
          			if (results) {
            			callback(undefined, results);
          			};
        		});
      		};
    	});
  	},
	
	//UC-204 & UC-205
  	checkUser(homeId, userId, callback) {
		console.log("checkUser called");
    	let sqlQuery = "SELECT * FROM home_administrators AS ha RIGHT JOIN studenthome AS sh ON ha.StudenthomeID = sh.ID WHERE ha.UserID = ? AND ha.StudenthomeID = ? OR sh.UserID = ? AND sh.ID = ?";

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		}
      		if (connection) {
        		connection.query(sqlQuery, [userId, homeId, userId, homeId], (error, result, fields) => {
          			connection.release();

          			if (error) {
            			const err = {
              				message: "query error: " + error.message,
              				errCode: 400,
            			};
            			callback(err, undefined);
          			}
          			if (result) {
            			if (!result.length) {
              				const err = {
                				message: userId + " not authorized",
                				errCode: 401,
              				};
              				callback(err, undefined);
            			} else {
              				callback(undefined, result);
            			}
          			}
        		});
      		}
    	});
  	},
	
	//UC-202
	getStudentHomes(name, city, callback) {
		console.log("getStudentHomes called");
    	let queryPart = "";
    	let errorMessage;

    	if (name && city) {
      		queryPart = "WHERE Name = '" + name + "' AND City = '" + city + "'";
      		errorMessage = "No studentHome was found on name: " + name + " and city: " + city;
    	} else if (name) {
      		queryPart = "WHERE Name = '" + name + "'";
      		errorMessage = "No studentHome was found on name: " + name;
    	} else if (city) {
      		queryPart = "WHERE City = '" + city + "'";
      		errorMessage = "No studentHome was found on city: " + city;
    	};

        let sqlQuery = "SELECT * FROM studenthome " + queryPart;

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, (error, results, fields) => {
          			connection.release();

    				if (error) {
            			const err = {
              				message: "query error: " + error.message,
              				errCode: 400,
            			};
            			callback(err, undefined);
          			};
          			if (results) {
            			if (!results.length && errorMessage) {
							console.log(results)
              				const err = {
                				message: errorMessage,
                				errCode: 404,
              				};
              				callback(err, undefined);
            			} else {
              			callback(undefined, results);
            			};
          			};
        		});
      		};
    	});
  	},
	
	//UC-203 & UC-204 & UC-205
  	getStudentHome(id, callback) {
		console.log("getStudentHome called");
    	let sqlQuery = "SELECT * FROM studenthome WHERE ID = " + id;

    	pool.getConnection((errCon, connection) => {
      		if (errCon) {
        		let error = {
          			message: errCon.message,
          			errCode: 400,
        		};
        		callback(error, undefined);
      		};
      		if (connection) {
        		connection.query(sqlQuery, (error, results, fields) => {
         			connection.release();
          			if (error) {
            			const err = {
              				message: "query error: " + error.message,
              				errCode: 400,
            			};
            			callback(err, undefined);
          			};
          			if (results) {
            			if (!results.length) {
              				const err = {
                				message: "no studenthome found with: " + id,
                				errCode: 404,
              				};
              				callback(err, undefined);
            			} else {
              				callback(undefined, results);
            			};
          			};
        		});
      		};
    	});
  	},
	
	//UC-206
	checkUser(homeId, userId, callback) {
		let sqlQuery = "SELECT * FROM home_administrators AS ha RIGHT JOIN studenthome AS sh ON ha.StudenthomeID = sh.ID WHERE ha.UserID = ? AND ha.StudenthomeID = ? OR sh.UserID = ? AND sh.ID = ?";
	
		pool.getConnection((errCon, connection) => {
			if (errCon) {
				let error = {
			  		message: "failed to check user",
			  		errCode: 400,
				};
				callback(error, undefined);
		  	}
		  	if (connection) {
				connection.query(sqlQuery, [userId, homeId, userId, homeId], (error, result, fields) => {
			  		connection.release();
	
			  		if (error) {
						const err = {
				  			message: error.message,
				  			errCode: 400,
						};
						callback(err, undefined);
			  		}
			  		if (result) {
						if (!result.length) {
				  			const err = {
								message: userId + " not authorized",
								errCode: 401,
				  		};
				  		callback(err, undefined);
						} else {
				  		callback(undefined, result);
						}
			  		}
				});
		  	}
		});
	},
	
	//UC-206
	addUserToHome(homeId, userId, callback) {
		console.log("addUserAdministrator called");
		let sqlQuery = "INSERT INTO home_administrators VALUES (?, ?)";
		console.log(homeId + " " + userId);

		pool.getConnection((errCon, connection) => {
			if (errCon) {
				let error = {
					message: "Connection Failed",
					errCode: 400,
				};
				callback(error, undefined);
			};
			if (connection) {
				connection.query(sqlQuery,[userId, homeId], (error, results, fields) => {
					connection.release();
			
					if (error) {
						logger.warn(error.message);
						const err = {
							message: "user already alrealdy added",
							errCode: 400,
						};
						callback(err, undefined);
					};
					if (results) {
						callback(undefined, results);
					};
				});
			};
		});
	},
};

