const assert = require("assert");
const jwt = require("jsonwebtoken");
const jwtKey = require("../DataLogic/databaseConfig").jwtSecretKey;
const pool = require("../DataLogic/databaseConnection");
const logger = require("../DataLogic/databaseConfig").logger;
const bcrypt = require("bcrypt");

module.exports = {
	validateToken(req, res, next) {
    	console.log("validate called");
    	const authHeader = req.headers.authorization;
    	
		if (!authHeader) {
      		logger.warn("authorization header missing");
      		res.status(401).json({
        		message: "authorization header missing",
        		datetime: new Date().toISOString(),
      		});
    	} else {
      		const token = authHeader.substring(7, authHeader.length);

      		jwt.verify(token, jwtKey, (err, payload) => {
        		if (err) {
          			logger.warn("not authorized");
          			res.status(401).json({
            			message: "not authorized",
            			datetime: new Date().toISOString(),
         			});
        		};
        		if (payload) {
          			logger.debug("token is valid", payload);
          			req.userId = payload.id;
          			next();
        		};
      		});
    	};
  	},

    register(req, res, next) {
		console.log("register called");

    	pool.getConnection((err, connection) => {
      		if (err) {
        		logger.error("error getting connection from pool: " + err.toString());
        		res.status(500).json({
					message: ex.toString(),
					datetime: new Date().toISOString()
				});
      		};
  
      		if (connection) {
        		let { firstname, lastname, email, studentnr, password } = req.body;
        		logger.debug(password);
				connection.query("INSERT INTO user (First_Name, Last_Name, Email, Student_Number, Password) VALUES (?, ?, ?, ?, ?)", [firstname, lastname, email, studentnr, password], (err, rows, fields) => {
					connection.release();

					logger.debug(password);

					if (err) {
						res.status(400).json({
							message: "register unsuccesful " + err.message,
							datetime: new Date().toISOString(),
						});
					} else {
						logger.trace(rows);
						const payload = {
							id: rows.insertIdS,
						};
						const userinfo = {
							username: firstname + " " + lastname,
							token: jwt.sign(payload, jwtKey, { expiresIn: "2h" }),
						};
						logger.debug("registered", userinfo);
						res.status(200).json(userinfo);
					};
					
				});
      		};
    	});
  	},

  	validateRegister(req, res, next) {
		console.log("validateRegister called");

    	try {
      		assert(typeof req.body.firstname === "string", "firstname is invalid or missing");
      		assert(typeof req.body.lastname === "string", "lastname is invalid or missing");
			assert(typeof req.body.email === "string", "email is invalid or missing");
      		assert(typeof req.body.password === "string", "password is invalid or missing");

      		const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      		assert(reg.test(req.body.email), "email was invalid format");

      		next();
    	} catch (ex) {
      		logger.debug("validateRegister error: ", ex.toString());
      		res.status(400).json({
				  message: ex.toString(),
				  datetime: new Date().toISOString()
			});
    	}
  	},
	  
  	login(req, res, next) {
		console.log("login called");

    	pool.getConnection((err, connection) => {
      		if (err) {
        		logger.error("error getting connection from pool");
        		res.status(500).json({
         			message: err.toString(),
          			datetime: new Date().toISOString(),
        		});
      		};
      		if (connection) {
        		connection.query("SELECT `ID`, `Email`, `Password`, `First_Name`, `Last_Name` FROM `user` WHERE `Email` = ?", [req.body.email], (err, rows, fields) => {
            		connection.release();

            		if (err) {
						logger.error("Error: ", err.toString());
						res.status(500).json({
							message: err.toString(),
							datetime: new Date().toISOString(),
						});
            		} else {

						if(rows.length) {
							bcrypt.compare(req.body.password, rows[0].Password).then((match) => {
								if (req.body.password === rows[0].Password) {
									logger.info("passwords DID match, sending valid token");
									const payload = {
										id: rows[0].ID,
									};
									const userinfo = {
										username: rows[0].First_Name + " " + rows[0].Last_Name,
										token: jwt.sign(payload, jwtKey, {
										expiresIn: "2h",
										}),
									};
									logger.debug("logged in, sending: ", userinfo);
									res.status(200).json(userinfo);
								} else {
									logger.info("password or mailadress is invalid");
									logger.info("rows password = " + (req.body.password));
									res.status(400).json({
										message: "password or mailadress is invalid",
										datetime: new Date().toISOString(),
									});
								};
							});
						} else {
							logger.info("no user found for email: " + req.body.email);
							res.status(400).json({
								message: "no user found for email: " + req.body.email,
								datetime: new Date().toISOString(),
							});
						};
            		};
          		});
      		};
    	});
  	},

  	validateLogin(req, res, next) {
		console.log("validateLogin called");

    	try {
      		assert(typeof req.body.email === "string", "email is invalid or missing");
      		assert(typeof req.body.password === "string", "password is invalid or missing");
      		
			const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		assert(reg.test(req.body.email), "email was invalid format.");
      		next();
    	} catch (ex) {
      		res.status(400).json({
				  message: ex.toString(),
				  datetime: new Date().toISOString()
			});
		};
  	},
};
