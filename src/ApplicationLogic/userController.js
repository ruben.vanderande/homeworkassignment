const database = require("../DataLogic/userDatabaseLogic");
const databaseMeals = require("../DataLogic/mealDatabaseLogic");

module.exports = {
    //UC-401
    addUserToMeals(req, res, next) {
        console.log("addUserToMeals called");
    	const studentHomeId = req.params.homeId;
    	const mealId = Number(req.params.mealId);
    	const userId = req.userId;
    	const signUpDate = new Date();

    	databaseMeals.getMeals(studentHomeId, (err, result) => {
      		if (err) {
        		next(err);
      		};
      		if (result) {
        		let meal = result.filter(function (e) {
					console.log("mealfilter");
          			return e.ID === mealId;
        		});

				if (!meal.length) {
					const error = {
						message: "no meals found with: " + mealId,
						errCode: 404,
					};
					next(error);
				} else {
					database.getUser(mealId, userId, (err, result) => {
						if (err) {
							console.log("error?");
							next(err);
						};
						if (result) {
							if (result.length) {
								const error = {
									message: "meal already assigned to " + userId,
									errCode: 404,
								};
								next(error);
							} else {
								if (meal[0].MaxParticipants >= result.length + 1) {
									database.addUserToMeal(studentHomeId, mealId, userId, signUpDate, (err, result) => {
										if (err) {
											next(err);
										};
										if (result) {
											console.log("user added");
											res.status(200).json({
												status: "success",
												result: {
													userId,
													studentHomeId,
													mealId,
													signUpDate,
												},
											});
										};
									});
								} else {
									const error = {
										message: meal[0].MaxParticipants,
										errCode: 400,
									};
									next(error);
								};
							};
						};
					});
				};
      		};
    	});
    },

    //UC-402
    removeUsersFromMeals(req, res, next) {
        console.log("removeUsersFromMeals called");
    	const studentHomeId = req.params.homeId;
   		const userId = req.userId;
    	const mealId = req.params.mealId;

    	databaseMeals.getMeals(studentHomeId, (err, result) => {
      		if (err) {
        		next(err);
      		};
      		if (result) {
        		let mealValidate = result.filter(function (e) {
          			return e.ID == mealId;
        		});
        		if (!mealValidate.length) {
          			const error = {
            			message: "no meals found with: " + mealId,
            			errCode: 404,
          			};
          			next(error);
        		} else {
          			database.getAllUsers(mealId, (err, result) => {
            			if (err) {
              				next(err);
            			};
            			if (result) {
              				if (!result.length) {
                			const error = {
                  				message: "no user found with: " + mealId,
                  				errCode: 404,
                			};
                			next(error);
              				} else {
                				let participantValidate = result.filter(function (e) {
                  					return e.UserID == userId;
               					});
                				if (!participantValidate.length) {
                  					const error = {
                    					message: "no user found with: " + userId,
                    					errCode: 404,
                  					};
                					next(error);
                				} else {
                  					database.deleteUser(studentHomeId, userId, mealId, (err, result) => {
                      					if (err) {
                        					next(err);
                      					};
                      					if (result) {
                        					console.log("user deleted from meal");
                        					res.status(200).json({
												status: "success",
												result: result.message
											});
                      					};
                    				});
                				};
              				};
            			};
          			});
        		};
      		};
    	});
    },

    //UC-403
    getUsersFromMeal(req, res, next) {
        console.log("getUsersFromMeal called");
        const mealId = Number(req.params.mealId);
    
        database.getAllUsers(mealId, (err, result) => {
          	if (err) {
            	next(err);
          	};
          	if (result) {
            	if (!result.length) {
              		const error = {
                		message: "no user found with: " + mealId,
                		errCode: 404,
              		};
              		next(error);
            	} else {
              		console.log("users received");
              		res.status(200).json({
						  status: "success",
						  result: result
					});
            	};
          	};
        });
    },

    //UC-401 & UC-404
    getUserFromMeal(req, res, next) {
        console.log("getUserFromMeal called");
        const mealId = Number(req.params.mealId);
        const participantId = req.params.participantId;
    
        database.getAllUsers(mealId, (err, result) => {
          	if (err) {
            	next(err);
          	};
          	if (result) {
            	if (!result.length) {
              		const error = {
                		message: "no user found with: " + mealId,
                		errCode: 404,
              		};
              		next(error);
            	} else {
              		let participantValidate = result.filter(function (e) {
                		return e.UserID == participantId;
              		});
              		if (!participantValidate.length) {
                		const error = {
                  			message: "no user found with: " +
                    		participantId,
                  			errCode: 404,
                		};
                		next(error);
              		} else {
                		database.getUser(mealId, participantId, (err, result) => {
							if (err) {
								next(err);
							};
							if (result) {
								console.log("user received");
								res.status(200).json({
									status: "success",
									result: result
								});
							};
                		});
              		};
            	};
          	};
        });
    },
};
