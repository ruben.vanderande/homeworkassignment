const assert = require("assert");
const database = require("../DataLogic/mealDatabaseLogic");

module.exports = {
    //UC-301
    mealAdd(req, res, next) {
        console.log("mealAdd called");
        const studentHomeId = req.params.homeId;
        const userId = req.userId;
        const meal = req.body;
    
        database.addMeal(studentHomeId, userId, meal, (err, result) => {
            if (err) {
                next(err);
            };
            if (result) {
                console.log("meal added");
                res.status(200).json({
                    status: "success",
                    result: {
                        id: result.insertId,
                        ...meal,
                    },
                });
            };
        });
    },

    //UC-302
    updateMeal(req, res, next) {
        console.log("updateMeal called");
        const studentHomeId = req.params.homeId;
        const userId = req.userId;
        const mealId = req.params.mealId;
        const meal = req.body;
    
        database.getMeals(studentHomeId, (err, result) => {
            if (err) {
                next(err);
            };
            if (result) {
                let mealValidate = result.filter(function (e) {
                return e.ID == mealId;
                });
                if (!mealValidate.length) {
                    const error = {
                        message: "no meals found with: " + mealId,
                        errCode: 404,
                    };
                next(error);
                } else {
                    database.updateMeal(
                        studentHomeId,
                        userId,
                        mealId,
                        meal,

                        (err, result) => {
                        if (err) {
                            next(err);
                        };
                        if (result) {
                            console.log("meal updated");
                            res.status(200).json({
                                status: "success",
                                result: {
                                    id: Number(mealId),
                                    ...meal,
                                },
                            });
                        };
                    });
                };
            };
        });
    },

    //UC-305
    deleteMeal(req, res, next) {
        console.log("deleteMeal called");
        const studentHomeId = req.params.homeId;
        const userId = req.userId;
        const mealId = req.params.mealId;
    
        database.getMeals(studentHomeId, (err, result) => {
            if (err) {
                next(err);
            };
            if (result) {
                let mealValidate = result.filter(function (e) {
                    return e.ID == mealId;
                });

                if (!mealValidate.length) {
                    const error = {
                        message: "no meals found with: " + mealId,
                        errCode: 404,
                    };
                    next(error);
                } else {
                    database.deleteMeal(studentHomeId, userId, mealId, (err, result) => {
                        if (err) {
                            next(err);
                        };
                        if (result) {
                            console.log("meal deleted");
                            res.status(200).json({
                                status: "success",
                                result: result.message
                            });
                        };
                    });
                };
            };
        });
    },

    //UC-301 & UC-302
    checkMeal(req, res, next) {
        console.log("checkMeal called");
        
        try {
            const { name, description, ingredients, allergyInfo, offerDate, price } = req.body;
            const date = new Date(offerDate);
            let check = false;

            if (date instanceof Date && !isNaN(date)) {
                check = true;
            };
    
            assert(check, "error with offerDate");
            assert(typeof name === "string", "error with name");
            assert(typeof description === "string", "error with description");
            assert(Array.isArray(ingredients), "error with ingredients");
            assert(ingredients.length, "error with ingredients");
            assert(typeof allergyInfo === "string","error with allergyInfo");
            assert(typeof price === "string", "error with price");
    
            const reg = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
            assert(reg.test(price), "error with price");
    
            next();
        } catch (err) {
            console.log(err.message);
            res.status(400).json({
                error: "400",
                message: "err.message"
            });
            next();
        };
    },

    //UC-303
    getAllMeals(req, res, next) {
        console.log("getAllMeals called");
        const studentHomeId = req.params.homeId;

        database.getMeals(studentHomeId, (err, result) => {
            if (err) {
                next(err);
            };
            if (result) {
                console.log(result);
                console.log("all meals received");
                res.status(200).json({
                    status: "success",
                    result: result
                });
            };
        });
    },

    //UC-304
    getMealId(req, res, next) {
        console.log("getMealId called");
        const studentHomeId = req.params.homeId;
        const mealId = Number(req.params.mealId);

        database.getMeals(studentHomeId, (err, result) => {
            if (err) {
                next(err);
            };
            if (result) {
                let meal = result.filter(function (e) {
                    return e.ID === mealId;
                });

                if (!meal.length) {
                    const error = {
                        message: "no meals found with: " + mealId,
                        errCode: 404,
                    };
                    next(error);
                } else {
                    console.log("meal received");
                    res.status(200).json({
                        status: "success",
                        result: meal
                    });
                };
            };
        });
    },
};

