const assert = require("assert");
const database = require("../DataLogic/studentHousingDatabaseLogic");

module.exports = {
    //UC-201
    studentHomeAdd(req, res, next) {
        console.log("studentHomeAdd called");
        const studentHome = req.body;
        const userId = req.userId;

        database.addStudentHousing(studentHome, userId, (err, result) => {
            if (err) {
                next(err);
            };
            if (result) {
                console.log("studenthome added");
                res.status(200).json({
                    status: "success",
                    result: {
                        id: result.insertId,
                        ...studentHome,
                    },
                });
            };
        });
    },

    //UC-204
    updateStudentHome(req, res, next) {
        console.log("updateStudentHome called");
        const homeId = req.params.homeId;
        const studentHome = req.body;
        const userId = req.userId;
    
        database.getStudentHome(homeId, (err, result) => {
          	if (err) {
            	next(err);
          	};
          	if (result) {
            	database.checkUser(homeId, userId, (error, result) => {
					if (error) {
						next(error);
					};
					if (result) {
					    database.updateStudentHome(homeId, studentHome,  userId, (err, result) => {
							if (err) {
								next(err);
							}
							if (result) {
								console.log("studenthome updated");
								res.status(200).json({
									status: "success",
									result: {
										id: Number(homeId),
										...studentHome,
									},
								});
							};
						});
					};
            	});
          	};
        });
    },

    //UC-205
    deleteStudentHome(req, res, next) {
        console.log("deleteStudentHome called");
        const homeId = req.params.homeId;
        const userId = req.userId;
    
        database.getStudentHome(homeId, (err, result) => {
          	if (err) {
            	next(err);
          	};
          	if (result) {
            	database.checkUser(homeId, userId, (error, result) => {
					if (error) {
						next(error);
					};
					if (result) {
						database.deleteStudentHome(homeId, userId, (error, resultDelete) => {
							if (error) {
								next(error);
							};
							if (resultDelete) {
								console.log("studenthome deleted");
								res.status(200).json({
									status: "success",
									result: result
								});
							};
						});
					};
            	});
          	};
        });
    },

    //UC-201 & UC-204
    checkStudentHome(req, res, next) {
        console.log("checkStudentHome called");

        try {
            const { name, address, houseNr, postalCode, city, phoneNr } = req.body;

            assert(typeof name === "string", "error with name");
            assert(typeof address === "string", "error with address");
            assert(typeof houseNr === "number", "error with houseNr");
            assert(typeof postalCode === "string","error with postalCode");
            assert(typeof phoneNr === "string", "error with phoneNr");
            assert(typeof city === "string", "error with city");
      
            const rege = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
            assert(rege.test(postalCode), "postalCode was invalid");

            const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            assert(re.test(phoneNr), "error with phoneNr");

            next();
        } catch (err) {
            console.log(err.message);
            res.status(400).json({
                error: "400",
                message: "err.message"
            });
            next();
        };
    },

    //UC-202
    getAllStudentHomes(req, res, next) {
        console.log("getAllStudentHomes called");
        const name = req.query.name;
        const city = req.query.city;
    
        database.getStudentHomes(name, city, (err, result) => {
          	if (err) {
            	next(err);
          	};
          	if (result) {
            	console.log("studenthomes received");
            	res.status(200).json({
					status: "success",
					result: result
				});
          	};
        });
    },

    //UC-203
    getStudentHome(req, res, next) {
        console.log("getStudentHome called");
        const homeId = req.params.homeId;

        database.getStudentHome(homeId, (err, result) => {
          	if (err) {
            	next(err);
          	};
          	if (result) {
            	console.log("studenthome received");
            	res.status(200).json({
					status: "success",
					result: result
				});
          	};
        });

        
    },

    //UC-206
    addUserToStudentHome(req, res, next) {
        const homeId = req.params.homeId;
        const userAddId = req.body.id;
        const userId = req.userId;
    
        if (!userAddId) {
            res.status(400).json({
                error: "Some error occurred",
                message: "Missing id in request body",
            });
        } else {
            database.checkUser(homeId, userId, (error, result) => {
                if (error) {
                next(error);
                }
                if (result) {
                    database.addUserToHome(homeId, userAddId, (error, result) => {
                        if (error) {
                            next(error);
                        }
                        if (result) {
                            console.log("user succesfully added");
                            res.status(200).json({
                                status: "success",
                                result: userAddId + " added",
                            });
                        }
                    }
                    );
                }
            });
        }
    },


    //This code is meant to be used for an array database.
    
    /*
    //UC-201
    StudentHomeAdd(res, req, next) {
        console.log("StudentHomeAdd called");
        const newStudentHome = req.body;

        console.log(req.body);

        database.add(newStudentHome, (err, result) => {
        if (err) {
            next(err);
        }

        if (result) {
            res.status(200).json(result);
        }
        });
    },

    //UC-204
    updateStudentHome(req, res, next) {
        console.log("updateStudentHome called");
        const id = req.params.homeId;
        const updateHousingInfo = req.body;

        database.update(id, updateHousingInfo, (err, result) => {
        if (err) {
            next(err);
        }

        if (result) {
            res.status(200).json(result);
        }
        });
    },

    //UC-205
    deleteStudentHome(req, res, next) {
        console.log("deleteStudentHome called");
        const id = req.params.homeId;

        database.delete(id, (err, result) => {
        if (err) {
            next(err);
        }

        if (result) {
            res.status(200).json(result);
        }
        });
    },

    //UC-201 & UC-204
    checkStudentHome(req, res, next) {
        console.log("checkStudentHome called");
        try {
        const { name, street, housenumber, postalcode, city, phonenumber } = req.body;
            
        assert(typeof name === "string");
        assert(typeof street === "string");
        assert(typeof housenumber === "number");
        assert(typeof postalcode === "string");
        assert(typeof city === "string");
        assert(typeof phonenumber === "number");

        if (!req.params.homeId) {
            let studentHome = database.getAll().filter(function (e) {
            return (
                e.street === street &&
                e.houseNr === housenumber &&
                e.postalCode === postalcode &&
                e.city === city
            );
            });
            assert.strictEqual(studentHome.length, 0, "e")
        } 
        
        next();
        } catch (err) {
            next({ 
            message: "Something in your input is wrong",
            errCode: 404
            });
        }
    },

    //UC-202
    getAllStudentHomes(req, res, next) {
        console.log("getAllStudentHomes called");
        const name = req.query.name;
        const city = req.query.city;
    
        database.getByName(name, city, (err, result) => {
          if (err) {
            next(err);
          }
          if (result) {
            console.log(result);
            res.status(200).json(result);
          }
        });
    },

    //UC-203
    getStudentHomeById(req, res, next) {
        console.log("getHomeById called");
        const id = req.params.homeId;

        database.getById(id, (err, result) => {
        if (err) {
            next(err);
        }

        if (result) {
            res.status(200).json(result);
        }
        });
    },
    */
};

