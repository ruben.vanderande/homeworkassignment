const express = require('express');
const studentHomeRouter = require ("./src/Routers/studentHomeRouter");
const mealRouter = require ("./src/Routers/mealRouter");
const userRouter = require ("./src/Routers/userRouter");
const authenticationRouter = require ("./src/Routers/authenticationRouter");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

app.all("*", (req, res, next) => {
  const reqMethod = req.method;
  const reqUrl = req.url;
  console.log("endpoint called: " + reqMethod + " " + reqUrl);
  next();
  }, (req, res, next) => {
  const reqMethod = req.method;
  const reqUrl = req.url;
  console.log(reqMethod + " request at " + reqUrl);
  next();
});


app.get("/api/info", function (req, res) {
  res.status(200).json({
    message: "Welcome to Rubens Javascript Express Server! Here you can connect to a studenthome-meals database via an api",
    info: "https://homework-assignment-js-server.herokuapp.com/api/info",
    sonarqube: "https://sonarqube.avans-informatica-breda.nl/dashboard?id=aefaewr2340ijaisdjah22"
  });
});

app.use("/api", authenticationRouter);
app.use("/api", studentHomeRouter);
app.use("/api", mealRouter);
app.use("/api", userRouter);

app.all("*", (req, res, next) => {
  console.log("Catch-all endpoint initiated")
  next({
      message: "Endpoint " + req.url + " does not exist", error: 401,
      });
  res.status(404).json(error);
});

app.use("*", (error, req, res, next) => {
  console.log("Errorhandler called!", error);
  res.status(error.error).json({
    error: error.message,
  });
});

app.listen(port, () => {
  console.log(`Server running at ${port}/`);
});

module.exports = app;